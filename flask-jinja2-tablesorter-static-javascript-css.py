Flask assumes templates and static directories exist (though here I specify the URL to not prefix "static")
# http://flask.pocoo.org/docs/api/#flask.Flask

The URL to generate the static file location could be in .py instead of the template .html
# Alternatively: http://flask.pocoo.org/docs/api/#flask.send_from_directory



# myflask.py
# static/asc.gif
# static/bg.gif
# static/desc.gif
# static/jquery-latest.js
# static/jquery.tablesorter.min.js
# static/style.css
# templates/myflask.html


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# python flask-file-read.py  * Running on http://0.0.0.0:5000/
from flask import Flask
from flask import render_template
app = Flask(__name__, static_folder='static', static_url_path='')


@app.route('/')
def get_license():
    try:
        response_items = [('apple', 'green'), ('banana', 'yellow'), ('orange', 'orange')]
        print len(response_items), 'response items'
        title = 'Flask Jinja2 Example'
        content = ''
        table_headers = ['Name', 'Color']
        return render_template('myflask.html', title=title, body_content=content, my_table_headers=table_headers,
                               results=response_items)
    except Exception as general_error:
        print general_error


if __name__ == '__main__':
    app.run('0.0.0.0')  # Werkzeug defaults to port 5000



- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# templates/myflask.html

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{{title}}</title>
    <link type="text/css" rel="stylesheet" href="{{ url_for('static', filename='style.css') }}" />
    <script type="text/javascript" src="{{ url_for('static', filename='jquery-latest.js') }}"></script>
    <script type="text/javascript" src="{{ url_for('static', filename='jquery.tablesorter.min.js') }}"></script>
</head>

<body>
    <table id="myTable" class="tablesorter">
    <thead>
    <tr>
        {% for h in my_table_headers %}
            <th>{{ h }}</th>
        {% endfor %}
    </tr>
    </thead>
    <tbody>
        {% for item in results %}
        <tr>
            <td>{{ item[0] }}</td><td>{{ item[1] }}</td>
        </tr>
        {% endfor %}
    </tbody>
    </table>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#myTable").tablesorter();
        });
    </script>

</body>
</html>