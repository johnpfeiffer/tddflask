# python flask-file-read.py  * Running on http://0.0.0.0:8080/
from flask import Flask, request
import os
	
app = Flask(__name__)

@app.route('/license')
def get_license():
	try:
		licenses = get_licenses_from_data_source()
		print len(licenses), 'licenses found'
		response = '\n<br />\n==>'.join(licenses)
	except Exception as error:
		response = str(error)
		return response

def get_licenses_from_data_source():
	with open('/tmp/example-licenses.log') as f:
		data = f.read()
		licenses = [item for item in data.split('==>') if item]  # TODO: use filter?
		return licenses

@app.route('/listing')
def get_files():
	try:
		current_file = os.path.realpath(__file__)
		current_directory = os.path.dirname(current_file)
		items = os.listdir(current_directory)
		print(items)
		return str(items)
	except Exception as error:
		response = str(error)
		return response

@app.route('/postdata', methods=['GET', 'POST'])
def login():
	if request.method == 'POST':
        # print(request.stream.read())  # works on binaries and does not cache but be careful if the data is too large
        searchword = str(request.args.get('key', ''))
        keyword = str(request.form.get('keyword', ''))  # after request.stream.read() this will be empty
        # print(request.data)  # only works if there is a content type

		return searchword + keyword
	else:
		return "POST some data"

if __name__ == '__main__':
	app.run('0.0.0.0', 8084, use_reloader=True)  # Werkzeug defaults to port 5000

# curl -X POST -d "keyword=blah" http://127.0.0.1:8084/postdata?key=foo