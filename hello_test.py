import unittest
import hello


class HelloTestCase(unittest.TestCase):

    def setUp(self):
        hello.app.config['TESTING'] = True
        self.app = hello.app.test_client()

    # def tearDown(self):

    def test_index_route(self):
        response = self.app.get('/', content_type='text/plaintext')
        self.assertEqual(200, response.status_code)
        self.assertEqual('Hello!', response.data)

    def test_not_found_404(self):
        response = self.app.get('/doesnotexist')
        self.assertEqual(404, response.status_code)


if __name__ == '__main__':
    unittest.main()

